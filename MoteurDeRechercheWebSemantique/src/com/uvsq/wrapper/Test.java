package com.uvsq.wrapper;

import com.acsis.view.Ontologie1;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.uvsq.dictionnaire.SynonymesParser;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class Test {
	
	public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
                /*
		//Structure des commentaires :
		//NomAttribut:baliseMere#BaliseFils:IdBalise:Ordre
		
		
		//Regles d'�criture des commentaires :
		
		// I :
		//Quand il sagit d'une balise table 
		//mettez table#table et une valeur quelconque pour ordre (ex : -1)

		// II :
		//Quand il sagit de <balise id="xxx">Valeur</balise> (ex : <p id="p1">Test</p>)
		//mettez balise#balise et une valeur quelconque pour ordre (ex : 0)
		//Pour : <p id="p1">Test</p> on aura => NomAttribut:p#p:IdBalise:0
		
		ArrayList<String> attributs = new ArrayList<>();
		ArrayList<String> commentaires = ReaderFile2.getContentFile("c:/catalogue-formation.txt",null);
		
		attributs.add("discipline");

		//commentaires.add("Discipline(s):table#table:details:-1");
		
		attributs.add("mention");
		//commentaires.add("Mention(s):table#table:details:-1");
		
		
		attributs.add("nom");
		//commentaires.add("UNDEFINED:div#h1:contenu_deco:1");
		
		attributs.add("phrase_auto");
		//commentaires.add("UNDEFINED:p#p:phrase_auto:0");
		
		attributs.add("alternance");
		
		String lien_resource = "http://www.uvsq.fr/master-2-professionnel-analyses-et-conception-de-systemes-d-information-surs-acsis--117236.kjsp?RH=ACCUEIL-FR";
		
		FormationWrapper formationWrapper = new FormationWrapper(null, attributs, commentaires,lien_resource);

		formationWrapper.parser();
                
                */
		
            
            //FormationWrapper fw = new FormationWrapper(args, null, null, null);
           // fw.getLienComposante("http://www.uvsq.fr/master-2-professionnel-analyses-et-conception-de-systemes-d-information-surs-acsis--117236.kjsp?RH=ACCUEIL-FR");
            
            //String chaine = "         TO   TO           ";
            
            //System.out.println("Chaine = "+chaine.trim());
            
            //SynonymesParser.getSysnonyme("spécificités de la formation", "C:/synonymes.xml"));
            
            
            Ontologie1 ontologie = new Ontologie1();
            ontologie.genererModel("ontologieGenerale.owl");
            ontologie.listeOntologie();

            //System.out.println(ontologie.getRelation("Formation","ACSIS"));
            //System.out.println(ontologie.getRelation("ACSIS","Formation"));
//            System.out.println(ontologie.getRelation("ACSIS","Master"));
//            System.out.println(ontologie.getRelation("Master","Formation"));
              System.out.println("rrrr"+ontologie.getRelation("Duree","Acsis"));
//            System.out.println(ontologie.getRelation("ACSIS","composante"));
            
	}

}
