/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uvsq.wrapper;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author Rachid
 */
public class WrapperControler {
    
    private String method;
    private String lien;
    
    public WrapperControler(String method, String lien){
        this.method = method.toLowerCase();
        this.lien = lien;
    }
    
    public String analiserRequete() throws IOException{
        switch (method) {
            case "propose":
                    return this.getLienComposante();
                
            case "contient":
                    return this.getLienComposante();
            
            case "is a property of":
                return lien;
            default:
                return "";
            }
        
    }
    
    public String getLienComposante() throws IOException {
            String l = "";
            Document doc = Jsoup.connect(lien).get();
            Element element = doc.getElementById("details");
            int longueur = element.getElementsByTag("tr").size();
            for (int j = 0; j < longueur; j++) {
                    String cle = element.select("th").get(j).text();
                    if(cle.equals("Composante(s)")){
                            System.out.println("href = "+element.select("td").get(j).select("a").first().attr("href"));
                            l = element.select("td").get(j).select("a").first().attr("href");
                    }
            }
            return l;
   }
    
}
