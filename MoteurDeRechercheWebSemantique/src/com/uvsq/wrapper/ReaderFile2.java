package com.uvsq.wrapper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class ReaderFile2 {
	
	
	public static ArrayList<String> getContentFile(String fichier,ArrayList<String> noms_attributs){
		
		ArrayList<String> liste = new ArrayList<>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(fichier)))
		{
 
			String sCurrentLine;
 
			while ((sCurrentLine = br.readLine()) != null) {
                                StringTokenizer tokenizer = new StringTokenizer(sCurrentLine, "$");
				//StringTokenizer tokenizer = new StringTokenizer(sCurrentLine, ":");
				String attr = tokenizer.nextToken();
				for (int i = 0; i < noms_attributs.size(); i++) {
					if(noms_attributs.get(i).equals(attr)){
						liste.add(sCurrentLine);
					}
				}
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return liste;
		
	}

}
