package com.uvsq.wrapper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileRader {

	public static ArrayList<String> getContentFile(String fichier) throws IOException{
		ArrayList<String> comments = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(fichier));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append('\n');
	            line = br.readLine();
	        }
	        //System.out.println("L = "+sb.toString().length());
	        String ch = sb.toString();
	        	 comments.add(ch);
	    } finally {
	        br.close();
	    }
		
		return comments;
	}
	
}
