package com.uvsq.dictionnaire;


public class ElementDictionnaire {

	private String mot_cle;
	private String domaine;
	private String lien;
	
	
	public ElementDictionnaire(String mot_cle, String domaine, String lien) {
		super();
		this.mot_cle = mot_cle;
		this.domaine = domaine;
		this.lien = lien;
	}
	
	
	public String getMot_cle() {
		return mot_cle;
	}
	
	
	public void setMot_cle(String mot_cle) {
		this.mot_cle = mot_cle;
	}
	
	
	public String getDomaine() {
		return domaine;
	}
	
	
	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}
	
	
	public String getLien() {
		return lien;
	}
	
	
	public void setLien(String lien) {
		this.lien = lien;
	}
	
	
}
