package com.uvsq.dictionnaire;


import com.uvsq.dictionnaire.ElementDictionnaire;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DictionnaireParser {
    
          public static ArrayList<ElementDictionnaire> parser(String fichier) throws ParserConfigurationException, SAXException, IOException{
               ArrayList<ElementDictionnaire> liste = new ArrayList<ElementDictionnaire>();
               DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
               DocumentBuilder builder = factory.newDocumentBuilder();
               Document document = builder.parse(fichier);
               Element racine = document.getDocumentElement();
               NodeList bases = racine.getElementsByTagName("element");
               String mot_cle = "";
               String classn = "";
               String lien = "";
               for (int i = 0; i < bases.getLength(); i++) {
                       Node base=bases.item(i);
                       NodeList elements=base.getChildNodes();
                       for (int j = 0; j < elements.getLength(); j++) {
                               Node enfant=elements.item(j);
                               if(enfant.getNodeName().equals("mot-cle"))mot_cle = enfant.getTextContent().toLowerCase();
                               if(enfant.getNodeName().equals("classe"))classn = enfant.getTextContent();
                               if(enfant.getNodeName().equals("lien"))lien = enfant.getTextContent();

                       }
                       ElementDictionnaire element = new ElementDictionnaire(mot_cle,classn,lien);
                       liste.add(element);

               }
              return liste;
          }

		
}

