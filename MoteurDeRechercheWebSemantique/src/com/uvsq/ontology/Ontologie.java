package com.uvsq.ontology;



import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOError;
import java.util.ArrayList;
import java.util.Iterator;

import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.Ontology;
import com.hp.hpl.jena.ontology.impl.OntologyImpl;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

public class Ontologie {

	public int code ;
	public String Errormessage;
    public OntModel Model = null;
    public String URIOntologieselectionee		= null; 
    public ArrayList<String> ontologieName		= new ArrayList<String>();
    public ArrayList<String> ontologieURI		= new ArrayList<String>();
    public ArrayList<String> ClassesList 		= new ArrayList<String>();
    public ArrayList<String> SubClassesList 	= new ArrayList<String>();
    public ArrayList<Individual> ListIndividual 	= new ArrayList<Individual>();
    public ArrayList<String> Instances			= new ArrayList<String>();
    
    public void  genererModel(String CheminOwl) {
		FileReader ModelReader	=null ;
		
		try {
			ModelReader	= new FileReader(CheminOwl) ;
			Model		= ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
			Model.read(ModelReader,null);
			code=00;
			Errormessage="Pas d'erreur";
		}
		//Fichier Non trouvé
		catch (FileNotFoundException e) {
			code=01 ;
			Errormessage		= "Fichier Non trouv�";
		}
		//Erreur de Fichier
		catch (IOError e) {
			code=10 ;
			Errormessage		= "Erreur d'entr�e/sortie sur le fichier";
		}
		catch (Exception e ){
			code=99;
			Errormessage		= "Erreur Inconnue";
		}
	}

    public void listeOntologie() {
		Iterator<Ontology> iter = Model.listOntologies();
		int i=0;
		while (iter.hasNext()) {
			OntologyImpl ontimpl= (OntologyImpl) iter.next();
			
			if (ontologieURI.contains(ontimpl.getURI().toString())==false){
				if (ontologieName.contains(ontimpl.getLocalName().toString())==false) {
				ontologieURI.add(ontimpl.getURI().toString());
				ontologieName.add(ontimpl.getLocalName());
				System.out.println(ontimpl.getURI().toString()+"    "+ontimpl.getLocalName());
				i++;
				}
			}
		}
	}
    public void listeClasses() {
		Iterator<OntClass> iter = Model.listClasses();
		String[] value={"hichiam","4545","54545","5454","5454","254534"};
		while (iter.hasNext()) {
			OntClass classes= (OntClass) iter.next();
			if (classes.getLocalName()!=null){
			if (classes.isRestriction()!=true){ //Il faut Enlever les Classes Restrictions
			 //createInstance(getAttributes(classes.getLocalName()));
			//ValeurPropriete(ontologieName.get(0), classes.getLocalName());
			  ExtendedIterator<OntProperty> lise=classes.listDeclaredProperties();
			
				ClassesList.add(classes.getLocalName());
				//System.out.println(classes.getLocalName());
				}
			}
		}
	}

    public void listeSubClasses(String URI ,String NomClasse) {
		SubClassesList.clear();
		if (NomClasse!=null && URI!=null) {
			OntClass classes= Model.getOntClass(URI+ '#'+ NomClasse);
			Iterator<OntClass>subclasseiter=classes.listSubClasses();
			while (subclasseiter.hasNext()){
				OntClass subclass= (OntClass) subclasseiter.next();
				SubClassesList.add(subclass.getLocalName());
			}
		}
	}
    public void listeInstances(String URI ,String NomClasse){
		Instances.clear();
		ListIndividual.clear();
		Iterator<Individual> Instance; 
		
			Instance	= Model.listIndividuals();
	
		while (Instance.hasNext()) {
			Individual Exemple				= Instance.next();
			
			if (Exemple.getURI().toString().equalsIgnoreCase(URI)==false){ // A Ajouter pour le multi ONTOLOGIE
				if (ListIndividual.contains(Exemple)==false) {
					ListIndividual.add(Exemple);
					String NomExemple		= Exemple.getLocalName();
					String Classe			= classeInstance(Exemple);
					System.out.println(Exemple.getLocalName()+"      "+Classe);
					if (NomClasse.equalsIgnoreCase("#"+Classe)){
						if (Instances.contains(NomExemple)==false){
							Instances.add(NomExemple);
						}
					}
				}
			}
		}
	}
    public ArrayList<String> getAttributes(String classeName){
    	Iterator iter = Model.listDatatypeProperties();
    	ArrayList<String> listeAttr=new ArrayList<String>();
    	while (iter.hasNext()) {
    	DatatypeProperty prop = (DatatypeProperty) iter.next();
    	
    	String propName = prop.getLocalName();
    	
    	String dom = "";
    	String rng = "";
    	if(prop.getDomain()!=null)
    	dom = prop.getDomain().getLocalName();
    	if(prop.getRange()!=null)
    	rng = prop.getRange().getLocalName();
    	if(dom.equals(classeName))
    	{
    	listeAttr.add(propName);
    
    	//System.out.println(propName +": \t("+dom+") \t -> ("+rng+") ");
    	}
    	}
    	
    	return listeAttr;
    }
   /* public ArrayList<DatatypeProperty> getAttributes(String classeName){
    	Iterator iter = Model.listDatatypeProperties();
    	ArrayList<DatatypeProperty> listeAttr=new ArrayList<DatatypeProperty>();
    	while (iter.hasNext()) {
    	DatatypeProperty prop = (DatatypeProperty) iter.next();
    	
    	String propName = prop.getLocalName();
    	
    	String dom = "";
    	String rng = "";
    	if(prop.getDomain()!=null)
    	dom = prop.getDomain().getLocalName();
    	if(prop.getRange()!=null)
    	rng = prop.getRange().getLocalName();
    	if(dom.equals(classeName))
    	{
    	listeAttr.add(prop);
    	
    	//System.out.println(propName +": \t("+dom+") \t -> ("+rng+") ");
    	}
    	}
    	
    	return listeAttr;
    }*/
    /*public Individual createInstance(OntClass classe,ArrayList<DatatypeProperty> listeAttr,String[] value){
		Individual individual=Model.createIndividual(ontologieName.get(0)+"#"+classe.getLocalName(),classe);
		Iterator<DatatypeProperty> iterator=listeAttr.iterator();
    	DatatypeProperty pro=null;
    	int i=0;
		while(iterator.hasNext()) {
    		pro=iterator.next();
    		Model.createStatement(individual,pro,Model.createLiteral(value[i]));
    		i++;
		}
		
		return individual;
		
	}*/
    /*
    public Object createInstance(ArrayList<String> listeAttr){
    	BeanGenerator bg = new BeanGenerator();
    	Iterator<String> iter=listeAttr.listIterator();
		while(iter.hasNext()) {
			String val=iter.next();
			System.out.println(val);
			bg.addProperty(val,String.class);
		}
		Object ob=bg.create();
		Class c=ob.getClass();
		java.lang.reflect.Field[] fields=c.getFields();
		for (int i = 0; i < fields.length; i++) {
			System.out.println(fields[i].getName());
		}
		Map beanMap = BeanMap.create(ob); 
		beanMap.put("type", "footest");
		
	
		return ob;
		
	}
    */
	private String classeInstance(Individual exemple) {
		// TODO Auto-generated method stub
		return exemple.getOntClass().getLocalName();
	}
	public StringBuffer ValeurPropriete(String URI, String NomObjet){
		StringBuffer ValeurdeProp	= new StringBuffer();
		Individual proprieteInstance		= Model.getIndividual(URI+ "#"+ NomObjet);
		
		if (proprieteInstance!=null){
			Iterator<Statement> Listpropval = proprieteInstance.listProperties();
			while (Listpropval.hasNext()) {
				
				Statement propval= Listpropval.next();
				
				int positionNProp=propval.getPredicate().toString().indexOf("#");
				
				String NomProp=(propval.getPredicate().toString()).substring(positionNProp+1);
				ValeurdeProp.append(NomProp+" : \t");
				int positionProp=propval.getAlt().toString().indexOf("#");
						
				if (positionProp==0){
					String ValeurProp=propval.getAlt().toString();
					ValeurdeProp.append(ValeurProp+"\n");
				}
				else {
					String ValeurProp=propval.getAlt().toString().substring(positionProp+1);
					ValeurdeProp.append(ValeurProp+"\n");
				}
			}
		}	return ValeurdeProp;		}


}
