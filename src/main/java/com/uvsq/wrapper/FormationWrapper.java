package com.uvsq.wrapper;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class FormationWrapper {

	private Object object;
	private ArrayList<String> noms_attributs;
	private ArrayList<String> commentaires_attributs;
	private String lien_resource;
	
	public FormationWrapper(){}

	public FormationWrapper(Object object, ArrayList<String> noms_attributs,
			ArrayList<String> commentaires_attributs, String lien_resource) {
		super();
		this.object = object;
		this.noms_attributs = noms_attributs;
		this.commentaires_attributs = commentaires_attributs;
		//this.commentaires_attributs = FileRader.getContentFile("c:/catalogue-formation.txt");
		this.lien_resource = lien_resource;
	}


	public String parser() throws IOException{
	
                String result = "<html>";
            
		Document doc = Jsoup.connect(lien_resource).get();
		
		for (int i = 0; i < commentaires_attributs.size(); i++) {
			
			String commentaire = commentaires_attributs.get(i);
			String nomAttribut = WrapperUtility.getNomAttribut(commentaire);
			String baliseMere = WrapperUtility.getBaliseMere(commentaire);
			String baliseFils = WrapperUtility.getBaliseFils(commentaire);
			String idBaliseMere = WrapperUtility.getIdBalise(commentaire);
			int ordre = WrapperUtility.getOrderBaliseInPageOrInBalise(commentaire);
                        String nomAttr = WrapperUtility.getNomPourAffichage(commentaire);
			
			Element element = doc.getElementById(idBaliseMere);
			
                        System.out.println("=================================="+nomAttribut+"===================================");
			
                        
			if(nomAttribut.equals("UNDEFINED")){
				//element =
				if(baliseMere.equals(baliseFils) && ordre==0){
						System.out.println(nomAttribut+" :\n"+element.text());
                                                result+="<font color=green>"+nomAttr+" : </font>"+element.text()+"<br>";
                                                //System.out.println("result = "+result);
				}
				else{
					System.out.println(nomAttribut+" :\n"+element.select(baliseFils).get(ordre-1).text());
                                        result+="<font color=green>"+nomAttr+" : </font>"+element.select(baliseFils).get(ordre-1).text()+"<br>";
                                        //System.out.println("result = "+result);
				}
			}
			else{
                                System.out.println("IN ELSE : "+nomAttribut);
				int longueur = element.getElementsByTag("tr").size();
				for (int j = 0; j < longueur; j++) {
					String cle = element.select("th").get(j).text();
                                        System.out.println("cle = "+cle);
					if(cle.equals(nomAttribut)){
						System.out.println(nomAttribut+" :\n"+element.select("td").get(j).text());
                                                result+="<font color=green>"+nomAttr+" : </font>"+element.select("td").get(j).text()+"<br>";
                                                //System.out.println("result = "+result);
						break;
					}
				}
				
			}
			/*
                        
                        if(nomAttribut.equals("UNDEFINED")){
				//element =
				if(baliseMere.equals(baliseFils) && ordre==0){
						System.out.println(nomAttribut+" :\n"+element.text());
                                                result+="<font color=green>"+nomAttr+" : </font>"+element.text()+"<br>";
                                                //System.out.println("result = "+result);
				}
				else{
					System.out.println(nomAttribut+" :\n"+element.select(baliseFils).get(ordre-1).text());
                                        result+="<font color=green>"+nomAttr+" : </font>"+element.select(baliseFils).get(ordre-1).text()+"<br>";
                                        //System.out.println("result = "+result);
				}
			}
			else{
                            
                            if(baliseMere.equals("table")){
				int longueur = element.getElementsByTag("tr").size();
				for (int j = 0; j < longueur; j++) {
					String cle = element.select("th").get(j).text();
					if(cle.equals(nomAttribut)){
						System.out.println(nomAttribut+" :\n"+element.select("td").get(j).text());
                                                result+="<font color=green>"+nomAttr+" : </font>"+element.select("td").get(j).text()+"<br>";
                                                //System.out.println("result = "+result);
						break;
					}
				}
                            }
                            else{
                                System.out.println(nomAttribut+" :\n"+element.select(baliseFils).get(ordre-1).text());
                                result+="<font color=green>"+nomAttr+" : </font>"+element.select(baliseFils).get(ordre-1).text()+"<br>";
                            }
				
			}
                        */
                        System.out.println("=================================="+nomAttribut+"===================================");
                        
		}
		//return this.object;
                return result+"</html>";
	}
        
	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public ArrayList<String> getNoms_attributs() {
		return noms_attributs;
	}

	public void setNoms_attributs(ArrayList<String> noms_attributs) {
		this.noms_attributs = noms_attributs;
	}

	public ArrayList<String> getCommentaires_attributs() {
		return commentaires_attributs;
	}

	public void setCommentaires_attributs(ArrayList<String> commentaires_attributs) {
		this.commentaires_attributs = commentaires_attributs;
	}

	public String getLien_resource() {
		return lien_resource;
	}

	public void setLien_resource(String lien_resource) {
		this.lien_resource = lien_resource;
	}
	
	public static String getNomLong(String nomComplet){
		int indexFin = nomComplet.indexOf('(');
		
		return nomComplet.substring(0, indexFin-1);
	}
	
	public static String getNomCourt(String nomComplet){
		int indexDebut = nomComplet.indexOf('(');
		int indexFin = nomComplet.indexOf(')');
		
		return nomComplet.substring(indexDebut+1, indexFin);
	}
	
}
