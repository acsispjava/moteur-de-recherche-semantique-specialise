package com.uvsq.wrapper;

import java.util.StringTokenizer;

public class WrapperUtility {
	
	//Forme des commentaires :
	//nom:NomAttribut:baliseMere#BaliseFils:IdBalise:Ordre
	//Ordre signifie : l'ordre de la balise fils dans la balise m�re

	public static String getNomAttribut(String commentaire){
		StringTokenizer tokenizer = new StringTokenizer(commentaire, "$");
		tokenizer.nextToken();
		return tokenizer.nextToken();
	}
	
	public static String getBaliseMere(String commentaire){
		StringTokenizer tokenizer = new StringTokenizer(commentaire, "$");
		tokenizer.nextToken();
		tokenizer.nextToken();
		String str = tokenizer.nextToken();
		tokenizer = new StringTokenizer(str,"#");
		return tokenizer.nextToken();
	}
	
	public static String getBaliseFils(String commentaire){
		StringTokenizer tokenizer = new StringTokenizer(commentaire, "$");
		tokenizer.nextToken();
		tokenizer.nextToken();
		String str = tokenizer.nextToken();
		tokenizer = new StringTokenizer(str,"#");
		tokenizer.nextToken();
		return tokenizer.nextToken();
	}
	
	public static String getIdBalise(String commentaire){
		StringTokenizer tokenizer = new StringTokenizer(commentaire, "$");
		tokenizer.nextToken();
		tokenizer.nextToken();
		tokenizer.nextToken();
		return tokenizer.nextToken();
	}
	
	public static int getOrderBaliseInPageOrInBalise(String commentaire){
		StringTokenizer tokenizer = new StringTokenizer(commentaire, "$");
		tokenizer.nextToken();
		tokenizer.nextToken();
		tokenizer.nextToken();
		tokenizer.nextToken();
		String str = tokenizer.nextToken();
		return Integer.parseInt(str);
	}
	
        public static String getNomPourAffichage(String commentaire){
            StringTokenizer tokenizer = new StringTokenizer(commentaire, "$");
            tokenizer.nextToken();
            tokenizer.nextToken();
            tokenizer.nextToken();
            tokenizer.nextToken();
            tokenizer.nextToken();
            return tokenizer.nextToken();
        }
        
}
