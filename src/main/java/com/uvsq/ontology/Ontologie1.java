package com.acsis.view;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOError;
import java.util.ArrayList;
import java.util.Iterator;

import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.Ontology;
import com.hp.hpl.jena.ontology.impl.OntologyImpl;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;


public class Ontologie1 {

	public int code ;
	public String Errormessage;
    public OntModel Model = null;
    public String URIOntologieselectionee		= null; 
    public ArrayList<String> ontologieName		= new ArrayList<String>();
    public ArrayList<String> ontologieURI		= new ArrayList<String>();
    public ArrayList<String> ClassesList 		= new ArrayList<String>();
    public ArrayList<String> SubClassesList 	= new ArrayList<String>();
    public ArrayList<Individual> ListIndividual 	= new ArrayList<Individual>();
    public ArrayList<String> Instances			= new ArrayList<String>();
    public ArrayList<String> listeAttr			= new ArrayList<String>();
    
    public void  genererModel(String CheminOwl) {
		FileReader ModelReader	=null ;
		
		try {
			ModelReader	= new FileReader(CheminOwl) ;
			Model		= ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
			Model.read(ModelReader,null);
			code=00;
			Errormessage="Pas d erreur";
		}
		//Fichier Non trouvé
		catch (FileNotFoundException e) {
			code=01 ;
			Errormessage		= "Fichier Non trouve";
		}
		//Erreur de Fichier
		catch (IOError e) {
			code=10 ;
			Errormessage		= "Erreur d'entree/sortie sur le fichier";
		}
		catch (Exception e ){
			code=99;
			Errormessage		= "Erreur Inconnue";
		}
	}

    public void listeOntologie() {
		Iterator<Ontology> iter = Model.listOntologies();
		int i=0;
		while (iter.hasNext()) {
			OntologyImpl ontimpl= (OntologyImpl) iter.next();
			
			if (ontologieURI.contains(ontimpl.getURI().toString())==false){
				if (ontologieName.contains(ontimpl.getLocalName().toString())==false) {
				ontologieURI.add(ontimpl.getURI().toString());
				ontologieName.add(ontimpl.getLocalName());
				i++;
				}
			}
		}
	}
    public void listeClasses() {
		Iterator<OntClass> iter = Model.listClasses();
		String[] value={"hichiam","4545","54545","5454","5454","254534"};
		while (iter.hasNext()) {
			OntClass classes= (OntClass) iter.next();
			if (classes.getLocalName()!=null){
			if (classes.isRestriction()!=true){ //Il faut Enlever les Classes Restrictions
			 //createInstance(getAttributes(classes.getLocalName()));
			//ValeurPropriete(ontologieName.get(0), classes.getLocalName());
			  ExtendedIterator<OntProperty> lise=classes.listDeclaredProperties();
			
				ClassesList.add(classes.getLocalName());
				//System.out.println(classes.getLocalName());
				}
			}
		}
	}
    public OntClass getClass(String className){
		Iterator<OntClass> iter = Model.listClasses();
		String[] value={"hichiam","4545","54545","5454","5454","254534"};
		while (iter.hasNext()) {
			OntClass classes= (OntClass) iter.next();
			if (classes.getLocalName()!=null){
			if (classes.isRestriction()!=true){ //Il faut Enlever les Classes Restrictions
			  if(classes.getLocalName().equals(className))return classes;
				}
			}
		}
		return null;
    	
    }

    public void listeSubClasses(String URI ,String NomClasse) {
		SubClassesList.clear();
		if (NomClasse!=null && URI!=null) {
			OntClass classes= Model.getOntClass(URI+ '#'+ NomClasse);
			Iterator<OntClass>subclasseiter=classes.listSubClasses();
			while (subclasseiter.hasNext()){
				OntClass subclass= (OntClass) subclasseiter.next();
				SubClassesList.add(subclass.getLocalName());
			}
		}
	}
    public String isSubclass(String NomClasse,String nomClasse2){
    	SubClassesList.clear();
        String result="";
        OntClass classes;
        Iterator<OntClass> subclasseiter;
        try {
            
        
		if (NomClasse!=null) {
			classes= Model.getOntClass(ontologieURI.get(0)+ '#'+ NomClasse);
			subclasseiter=classes.listSubClasses();
                         
			while (subclasseiter.hasNext()){
				OntClass subclass= (OntClass) subclasseiter.next();
                                
                                
				if(subclass.getLocalName().equals(nomClasse2)){
                                    result=NomClasse+":"+nomClasse2;
                                    
                                   }
                                if(result==""){
                                     result= isSubclass(subclass.getLocalName(), nomClasse2);
                                }
                                
			}
                        if(result!=""){
                            result=NomClasse+":"+nomClasse2;
                        }
                        
                       
                        }
		
                } catch (Exception e) {
                    System.out.println(result+"hichamm");
        }
		return result;
    }
   public String getFirstSuperClass(String className){
         boolean trouve=true;
    	String classnme=className;
        try {
           
       
    	do{
    		classnme=getSuperClass(classnme).getLocalName();
    		      
    		if(getClass(classnme).getSuperClass()==null)trouve=false;
    		
    		
    	}while(trouve);
        } catch (Exception e) {
            return className;
       }
        return classnme;
    }
    public ArrayList<String> getAttributes(String classeName){
    	listeAttr.clear();
    	boolean trouve=true;
    	String classnme=classeName;
    	ArrayList<String> liste=null;
    	do{
    		liste=getAttribute(classnme);
    		System.out.println(liste.size());
    		if(getClass(classnme).getSuperClass()==null)trouve=false;
    		else
    		classnme=getClass(classnme).getSuperClass().getLocalName();
    		
    	}while(trouve);
    	this.listeAttr=liste;
    	return this.listeAttr;
    }
    public ArrayList<String> getAttribute(String classeName){
    	
    		Iterator iter = Model.listDatatypeProperties();
        	
        	while (iter.hasNext()) {
        		DatatypeProperty prop = (DatatypeProperty) iter.next();
    			String propName = prop.getLocalName();
                String dom = "";
    			String rng = "";
    			if(prop.getDomain()!=null)
    				dom = prop.getDomain().getLocalName();
    			if(prop.getRange()!=null)
    				rng = prop.getRange().getLocalName();
		    	if(dom.equals(classeName))
		    	{
		    		this.listeAttr.add(propName);
		    		System.out.println(propName);
		    		
		    	}
        	}
    	
    	return this.listeAttr;
    	
    }
    public void listeInstances(String URI ,String NomClasse){
		Instances.clear();
		ListIndividual.clear();
		Iterator<Individual> Instance; 
		
			Instance	= Model.listIndividuals();
	
		while (Instance.hasNext()) {
			Individual Exemple				= Instance.next();
			
			if (Exemple.getURI().toString().equalsIgnoreCase(URI)==false){ // A Ajouter pour le multi ONTOLOGIE
				if (ListIndividual.contains(Exemple)==false) {
					ListIndividual.add(Exemple);
					String NomExemple		= Exemple.getLocalName();
					String Classe			= classeInstance(Exemple);
					System.out.println(Exemple.getLocalName()+"      "+Classe);
					if (NomClasse.equalsIgnoreCase("#"+Classe)){
						if (Instances.contains(NomExemple)==false){
							Instances.add(NomExemple);
						}
					}
				}
			}
		}
	}

    private String classeInstance(Individual exemple) {
		// TODO Auto-generated method stub
		return exemple.getOntClass().getLocalName();
	}
    public OntClass getSuperClass(String className){
         
        return getClass(className).getSuperClass();
    }

    public String getRelation(String firstNoeud,String secondNoeud){
        String result1=getRelationSimilaire(firstNoeud, secondNoeud);
        if(result1==""){
            result1=getRelationSimilaire(secondNoeud, firstNoeud);
            
        }
        String result3=getRelationProperty(firstNoeud, secondNoeud);
        String result2=getRelationDifferent(firstNoeud, secondNoeud);
        if(result2!="")return result2;
        else if(result1!="")return result1;
        else if(result3!="")return result3;
        else return "Undefined";
    }
    public String getRelationSimilaire(String firstNoeud,String secondNoeud){
        
        
        return isSubclass(firstNoeud, secondNoeud);
        
    }
    public String isProperty(String proertyName){
        Iterator iter = Model.listDatatypeProperties();
        	
        	while (iter.hasNext()) {
        		DatatypeProperty prop = (DatatypeProperty) iter.next();
    			String propName = prop.getLocalName();
                if(propName.equals(proertyName)){
                    return prop.getDomain().getLocalName();
                }
                }
    	return "";
        
    }
    public String getRelationProperty(String firstNoeud,String secondNoeud){
        String classOfprop="";
        String fisrtSuperClassName="";
        String pro="";
        String noeud="";
        String result="";
        if(isProperty(firstNoeud)!=""){
            pro=firstNoeud;
            noeud=secondNoeud;
            classOfprop=isProperty(firstNoeud);
          
        }else if(isProperty(secondNoeud)!=""){
             pro=secondNoeud;
            noeud=firstNoeud;
            classOfprop=isProperty(secondNoeud);
        }
        fisrtSuperClassName=getFirstSuperClass(classOfprop);
        System.out.println(pro+"hichammm"+noeud);
        if(getFirstSuperClass(noeud).equals(classOfprop) && noeud!=""){
            result=pro+":is a property of:"+noeud;
        }
        return result;
        
    }
    public String getRelationDifferent(String firstNoeud1,String secondNoeud1){
        String firstNoeud=getFirstSuperClass(firstNoeud1);
        String secondNoeud=getFirstSuperClass(secondNoeud1);
        System.err.println(firstNoeud+"hciham"+secondNoeud);
        Iterator iter = Model.listObjectProperties();
       String relation="";
       while (iter.hasNext()) {
            ObjectProperty prop = (ObjectProperty) iter.next();
            String propName = prop.getLocalName();
            String rng = "";
            String mot1="";
            String mot2="";
            try {
            String dom = prop.getDomain().getLocalName();
                System.err.println(dom+"hichammmmmmmmmmmmm");
            
                 
            	mot1=getSuperClass(firstNoeud).getLocalName();
            	
			} catch (Exception e) {
				// TODO: handle exception
				mot1=firstNoeud;
				
			}
            try {
               mot2=getSuperClass(secondNoeud).getLocalName();
           } catch (Exception ex) {
               mot2=secondNoeud;
           }
            
           
           
            try {
                 if((prop.getDomain().getLocalName().equals(mot1) && prop.getRange().getLocalName().equals(mot2)))
            relation=firstNoeud1+":"+propName+":"+secondNoeud1;
            else if(prop.getDomain().getLocalName().equals(mot2) && prop.getRange().getLocalName().equals(mot1))
             relation=secondNoeud1+":"+propName+":"+firstNoeud1;
               
           } catch (Exception ex) {
               
           }
           
            
        }
       
        return relation ;
    }
}
