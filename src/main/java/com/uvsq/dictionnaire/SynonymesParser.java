/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uvsq.dictionnaire;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Rachid
 */
public class SynonymesParser {
    
    public static ArrayList<String> parser(String fichier) throws ParserConfigurationException, SAXException, IOException{
            ArrayList<String> liste = new ArrayList<String>();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(fichier);
            Element racine = document.getDocumentElement();
            NodeList bases = racine.getElementsByTagName("element");
            for (int i = 0; i < bases.getLength(); i++) {
                    Node base=bases.item(i);
                    NodeList elements=base.getChildNodes();
                    for (int j = 0; j < elements.getLength(); j++) {
                            Node enfant=elements.item(j);
                            if(enfant.getNodeName().equals("synonyme")){
                                liste.add(enfant.getTextContent());
                            }

                    }

            }
            Collections.sort(liste.subList(1, liste.size()));
            return liste;
       }
    
     public static ArrayList<ElementSynonyme> parser2(String fichier) throws ParserConfigurationException, SAXException, IOException{
               ArrayList<ElementSynonyme> liste = new ArrayList<ElementSynonyme>();
               DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
               DocumentBuilder builder = factory.newDocumentBuilder();
               Document document = builder.parse(fichier);
               Element racine = document.getDocumentElement();
               NodeList bases = racine.getElementsByTagName("element");
               String mot = "";
               String synonyme = "";
               for (int i = 0; i < bases.getLength(); i++) {
                       Node base=bases.item(i);
                       NodeList elements=base.getChildNodes();
                       for (int j = 0; j < elements.getLength(); j++) {
                               Node enfant=elements.item(j);
                               if(enfant.getNodeName().equals("mot")) mot = enfant.getTextContent().toLowerCase();
                               if(enfant.getNodeName().equals("synonyme"))synonyme = enfant.getTextContent();

                       }
                       ElementSynonyme element = new ElementSynonyme(mot,synonyme);
                       liste.add(element);

               }
              return liste;
          }
    
    public static String getSysnonyme(String mot_cle, String fichier) throws ParserConfigurationException, SAXException{
        ArrayList<ElementSynonyme> liste = null;
        try {
            liste = parser2(fichier);
        } catch (IOException ex) {
            Logger.getLogger(SynonymesParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < liste.size(); i++) {
            if(liste.get(i).getSynonyme().toLowerCase().equals(mot_cle)){
                return liste.get(i).getMot();
            }
        }
        return "";
    }
    
}
