/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uvsq.dictionnaire;

/**
 *
 * @author Rachid
 */
public class ElementSynonyme {
    
    private String mot;
    private String synonyme;
	
    public ElementSynonyme(String mot, String synonyme) {
        super();
        this.mot = mot;
        this.synonyme = synonyme;
    }

    public String getMot() {
        return mot;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }

    public String getSynonyme() {
        return synonyme;
    }

    public void setSynonyme(String synonyme) {
        this.synonyme = synonyme;
    }
    
    
    
}
