package com.uvsq.dictionnaire;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class DictionnaireUtility {

	public static ArrayList<ElementDictionnaire> chargerDictionnaire(String cheminDico){
		ArrayList<ElementDictionnaire> dictionnaire = new ArrayList<ElementDictionnaire>();
		
        //dictionnaire.add(new ElementDictionnaire("acsis", "Master", "http://www.uvsq.fr/master-2-professionnel-analyses-et-conception-de-systemes-d-information-surs-acsis--117236.kjsp?RH=ACCUEIL-FR"));
        dictionnaire.add(new ElementDictionnaire("acsis", "Master", "http://localhost/acsis2.html"));
        dictionnaire.add(new ElementDictionnaire("master", "Master", "http://www.uvsq.fr/les-masters-234462.kjsp?RH=FORM_5&RF=FORM_5"));
        dictionnaire.add(new ElementDictionnaire("dsme", "Master", "http://www.uvsq.fr/master-2-recherche-et-professionnel-dimensionnement-des-structures-mecaniques-dans-leur-environnement-dsme--117701.kjsp?RH=FORM_5"));
        dictionnaire.add(new ElementDictionnaire("gse", "Lisence", "http://localhost/gse.html"));
        dictionnaire.add(new ElementDictionnaire("sacim", "Master", "http://www.uvsq.fr/master-1-professionnel-ingenierie-de-la-culture-et-de-la-communication-mediation-des-savoirs-scientifiques-organisation-d-evenements-et-d-espaces-culturels-sacim--114470.kjsp?RH=FORM_5"));
        dictionnaire.add(new ElementDictionnaire("etadd", "Master", "http://www.uvsq.fr/master-2-recherche-economie-theorique-et-appliquee-du-developpement-durable-etadd--116059.kjsp?RH=FORM_5"));
		return dictionnaire;
	}
	
	public static int nombreMotCle(String mot_cle){
		StringTokenizer tokenizer = new StringTokenizer(mot_cle);
		return tokenizer.countTokens();
	}
	
	public static String getMotCle(String mot_cle_complet, int position){
            String[] result = mot_cle_complet.split("\\s");
	    return result[position-1];
	}
	
	//Cette m�thode permet de v�rifier le cas suivant par exemple : "master acsis" ou "acsis master" 
	
	public static String getLinkNodeValue(String mot_cle1, String mot_cle2,ArrayList<ElementDictionnaire> dico){
		for (int i = 0; i < dico.size(); i++) {
			if((dico.get(i).getMot_cle().equals(mot_cle1.toLowerCase()) && dico.get(i).getDomaine().equals(mot_cle2.toLowerCase())) || (dico.get(i).getMot_cle().equals(mot_cle2.toLowerCase()) && dico.get(i).getDomaine().equals(mot_cle1.toLowerCase()))){
				return dico.get(i).getLien();
			}
		}
		return "";
	}
	
	public static String getClasseNodeValue(String mot_cle1, String mot_cle2,ArrayList<ElementDictionnaire> dico){
		for (int i = 0; i < dico.size(); i++) {
			if((dico.get(i).getMot_cle().equals(mot_cle1.toLowerCase()) && dico.get(i).getDomaine().equals(mot_cle2.toLowerCase())) || (dico.get(i).getMot_cle().equals(mot_cle2.toLowerCase()) && dico.get(i).getDomaine().equals(mot_cle1.toLowerCase()))){
				return dico.get(i).getDomaine();
			}
		}
		return "";
	}
	
	public static boolean verifyNodeAndValueFor2Words(String mot_cle1, String mot_cle2,ArrayList<ElementDictionnaire> dico){
		for (int i = 0; i < dico.size(); i++) {
			if((dico.get(i).getMot_cle().equals(mot_cle1.toLowerCase()) && dico.get(i).getDomaine().equals(mot_cle2.toLowerCase())) || (dico.get(i).getMot_cle().equals(mot_cle2.toLowerCase()) && dico.get(i).getDomaine().equals(mot_cle1.toLowerCase()))){
				return true;
			}
		}
		return false;
	}
	
	public static String isNode(String classe, ArrayList<ElementDictionnaire> dico){
		
		for (int i = 0; i < dico.size(); i++) {
			if(classe.equals(dico.get(i).getDomaine().toLowerCase())){
                                System.out.println("Yeeees is node ! ");
				return dico.get(i).getLien();
			}
		}
                System.out.println("Nooooooooo is not node ! ");
		return "";
	}
	
        public static boolean existeInDictionnary(ArrayList<ElementDictionnaire> dico, String mot_cle){
		
		for (int i = 0; i < dico.size(); i++) {
			if(mot_cle.equals(dico.get(i).getDomaine().toLowerCase()) || mot_cle.equals(dico.get(i).getMot_cle().toLowerCase())){
				return true;
			}
		}
		return false;
	}
        
        public static String getResultatFromDictionnaire2Nodes(ArrayList<ElementDictionnaire> dico,String mot_cle1,String mot_cle2){
            
            String result = "<html>";
            result+="<h3>Résultat pour <font color=orange><i><b>"+mot_cle1+"</b></i></font> : </h3>";
            for (int i = 0; i < dico.size(); i++) {
                if(dico.get(i).getDomaine().equals(mot_cle1)){
                    result+="<font color=green>"+dico.get(i).getMot_cle().toUpperCase()+" : </font>"+"<a href="+dico.get(i).getLien()+">"+dico.get(i).getLien()+"</a><br>";
                }
            }
            
            result+="<h3>Résultat pour <font color=orange><i><b>"+mot_cle2+"</b></i></font> : </h3>";
            for (int i = 0; i < dico.size(); i++) {
                if(dico.get(i).getDomaine().equals(mot_cle2)){
                    result+="<font color=green>"+dico.get(i).getMot_cle().toUpperCase()+" : </font>"+"<a href="+dico.get(i).getLien()+">"+dico.get(i).getLien()+"</a><br>";
                }
            }
            return result+"</html>";
        }
        
	public static String contientElement(ArrayList<ElementDictionnaire> dico, String mot_cle){
            mot_cle = mot_cle.toLowerCase();
        for (int i = 0; i < dico.size(); i++) {
                if(dico.get(i).getMot_cle().equals(mot_cle)){
                        System.out.println("in contientElement : oui il contient l'element !");
                        return dico.get(i).getLien();
                }
        }
        System.out.println("in contientElement : Non il ne contient l'element !");
        return "";
	}
	
        public static String getClasseByMotCle(ArrayList<ElementDictionnaire> dico, String mot_cle){
        for (int i = 0; i < dico.size(); i++) {
                if(dico.get(i).getMot_cle().equals(mot_cle)){
                        return dico.get(i).getDomaine();
                }
        }
        return "";
        }
	
        public static String getResultFromDictionnaire(ArrayList<ElementDictionnaire> dico, String domaine){
            String result = "<html><head></head><body>";
            for (int i = 0; i < dico.size(); i++) {
                if(dico.get(i).getDomaine().equals(domaine)){
                    result+="<font color=red>"+dico.get(i).getMot_cle().toUpperCase()+" : </font>"+"<a href="+dico.get(i).getLien()+">"+dico.get(i).getLien()+"</a><br>";
                }
            }
            return result+"</body></html>";
        }
	 
}
