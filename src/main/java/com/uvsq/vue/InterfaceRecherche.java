/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uvsq.vue;
import com.acsis.view.Ontologie1;
import com.uvsq.dictionnaire.DictionnaireControler;
import com.uvsq.dictionnaire.DictionnaireParser;
import com.uvsq.dictionnaire.DictionnaireUtility;
import com.uvsq.dictionnaire.ElementDictionnaire;
import com.uvsq.dictionnaire.SynonymesParser;
import com.uvsq.wrapper.FormationWrapper;
import com.uvsq.wrapper.ReaderFile2;
import com.uvsq.wrapper.WrapperControler;

import java.awt.Color;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 *
 * @author Rachid
 */
public class InterfaceRecherche extends javax.swing.JFrame {

    /**
     * Creates new form InterfaceRecherche
     */
    
    ArrayList<ElementDictionnaire> dictionnaire;
    String mot_cle;
    Ontologie1 ontologie;
    ArrayList<String> noms_attributs;
    ArrayList<String> comments;
    ArrayList<String> words;
    
    public InterfaceRecherche() {
        initComponents();
        init();
    }

    private void init(){
        
            this.setLocationRelativeTo(null);
            this.setTitle("Moteur de recherche Web Semantique");
          //  this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("index.jpg")));
            
         try {
            words = SynonymesParser.parser("synonymes.xml");
            
            AutoSuggestor autoSuggestor = new AutoSuggestor(jTextField1, this, null, Color.WHITE.brighter(), Color.BLUE, Color.RED, 0.75f) {
            @Override
            boolean wordTyped(String typedWord) {

                //create list for dictionary this in your case might be done via calling a method which queries db and returns results as arraylist
                //ArrayList<String> words = new ArrayList<>();
               
                setDictionary(words);
                //addToDictionary("bye");//adds a single word

                return super.wordTyped(typedWord);//now call super to check for any matches against newest dictionary
            }
        }; 
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(InterfaceRecherche.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(InterfaceRecherche.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(InterfaceRecherche.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jTextField1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon-recherche32.png"))); // NOI18N
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton1MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jButton1MouseReleased(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel2.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jScrollPane1.setViewportView(jLabel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(104, 104, 104)
                .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(93, 93, 93))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 322, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
  
        dictionnaire = new ArrayList<ElementDictionnaire>();
        //Remplissage de notre dictionnaire
          /* 
           dictionnaire.add(new ElementDictionnaire("acsis", "master", "http://localhost/acsis2.html"));
           dictionnaire.add(new ElementDictionnaire("dsme", "master", "http://www.uvsq.fr/master-2-recherche-et-professionnel-dimensionnement-des-structures-mecaniques-dans-leur-environnement-dsme--117701.kjsp?RH=FORM_5"));
           dictionnaire.add(new ElementDictionnaire("gse", "lisence", "http://localhost/gse.html"));
           dictionnaire.add(new ElementDictionnaire("sacim", "master", "http://www.uvsq.fr/master-1-professionnel-ingenierie-de-la-culture-et-de-la-communication-mediation-des-savoirs-scientifiques-organisation-d-evenements-et-d-espaces-culturels-sacim--114470.kjsp?RH=FORM_5"));
           dictionnaire.add(new ElementDictionnaire("etadd", "master", "http://www.uvsq.fr/master-2-recherche-economie-theorique-et-appliquee-du-developpement-durable-etadd--116059.kjsp?RH=FORM_5"));
           dictionnaire.add(new ElementDictionnaire("gcgp", "dut", "http://www.uvsq.fr/dut-genie-chimique-genie-des-procedes-option-bio-procedes-gcgp--15443.kjsp?RH=FORM6"));
        */
        try {
            dictionnaire = DictionnaireParser.parser("dictionnaire.xml");
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(InterfaceRecherche.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(InterfaceRecherche.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(InterfaceRecherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ontologie = new Ontologie1();
        ontologie.genererModel("ontologieGenerale(1).owl");
        
        System.out.println("Siiiiiiiiiiiize = "+dictionnaire.size());

    }//GEN-LAST:event_formWindowOpened

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        /*if(jButton1.getText().equals("Nouvelle recherche")){
            jTextField1.setText("");
            jLabel3.setText("");
            jLabel2.setText("");
            jButton1.setText("Lancer la recherche");
        }*/

            //trim() : Permet d'enlever les espaces de début et du fin de la chaine saisie par l'utilisateur
            mot_cle = jTextField1.getText().toLowerCase().trim();
            String mot_synonyme = "";
            try {
                mot_synonyme = SynonymesParser.getSysnonyme(mot_cle, "synonymes.xml");
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(InterfaceRecherche.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(InterfaceRecherche.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            System.out.println("mot_synonyme = "+mot_synonyme);
            
            if( ! mot_synonyme.equals("")) mot_cle = mot_synonyme;
            
            if(mot_cle.equals("")){
                JOptionPane.showMessageDialog(this, "Vous devez spécifier un critère de recherche !", "Attention",JOptionPane.ERROR_MESSAGE);
            }
            else{

                String lienFormation = "";
                String domaine = "";
                String attribut = "";
                
                int nombreMotCle = DictionnaireUtility.nombreMotCle(mot_cle);

                System.out.println("nombreMotCle = "+nombreMotCle);

                if(nombreMotCle == 1){
                    ArrayList<String> result = new ArrayList<>();
                    result = DictionnaireControler.recherche1MotCle(mot_cle, dictionnaire);
                    System.out.println("result.size() = "+result.size());
                    if(result.size()==1){
                        String msg = "<html>Résultat de recherche pour <font color=red>"+mot_cle.toUpperCase()+"</font> </html>:";
                        jLabel3.setText(msg);
                        jLabel2.setText(result.get(0));
                        jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                        //jButton1.setText("Nouvelle recherche");
                        return;
                    }
                    else{
                        if(result.size()==2){
                            domaine = result.get(0);
                            lienFormation = result.get(1);
                        }
                    }

                }
                else{
                    if(nombreMotCle == 2){
                        
                        String mot_cle1 = DictionnaireUtility.getMotCle(mot_cle, 1);
                        String mot_cle2 = DictionnaireUtility.getMotCle(mot_cle, 2);

                        ontologie.listeOntologie(); 
                        //String relation = ontologie.getRelation("Master","Formation");
                        mot_cle1 = mot_cle1.substring(0, 1).toUpperCase() + mot_cle1.substring(1);
                        mot_cle2 = mot_cle2.substring(0, 1).toUpperCase() + mot_cle2.substring(1);
                        System.out.println("mot_cle1 ******$>"+mot_cle1);
                        System.out.println("mot_cle2 ******$>"+mot_cle2);
                        
                        //String relation = ontologie.getRelation("domaine","Acsis");
                        String relation = ontologie.getRelation(mot_cle1,mot_cle2);

                        String domaine1 = "";
                        String domaine2 = "";
                        
                        if(relation.equals("Undefined")){
                            System.out.println("Aucune relation entre les deux ! ");
                        }
                        else{
                            StringTokenizer tokenizer = new StringTokenizer(relation, ":");
                            int nbrElements = tokenizer.countTokens();
                            if(nbrElements==2){
                                tokenizer.nextToken();
                                domaine1 = tokenizer.nextToken();
                                relation = "";
                                //======================Duplication du code du cas d'un seul mot clé===============================
                                    ArrayList<String> result = new ArrayList<>();
                                    result = DictionnaireControler.recherche1MotCle(domaine1, dictionnaire);
                                    System.out.println("result.size() = "+result.size());
                                    if(result.size()==1){
                                        String msg = "<html>Résultat de recherche pour <font color=red>"+mot_cle.toUpperCase()+"</font> </html>:";
                                        jLabel3.setText(msg);
                                        jLabel2.setText(result.get(0));
                                        jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                                        //jButton1.setText("Nouvelle recherche");
                                        return;
                                    }
                                    else{
                                        if(result.size()==2){
                                            domaine = result.get(0);
                                            lienFormation = result.get(1);
                                        }
                                    }
                                //======================Fin duplication du code du cas d'un seul mot clé===============================
                            }
                            else{ //nbrElements == 3
                                domaine1 = tokenizer.nextToken();
                                relation = tokenizer.nextToken();
                                domaine2 = tokenizer.nextToken();
                                
                                String link = DictionnaireUtility.contientElement(dictionnaire, domaine2);
                                if(link.equals("")){
                                    JOptionPane.showMessageDialog(this, "Aucun résultat trouvé pour "+mot_cle.toUpperCase()+" !", "Information",JOptionPane.INFORMATION_MESSAGE);
                                    return;
                                }
                                else{
                                    WrapperControler wc = new WrapperControler(relation, link);
                                    String ret = "";
                                    try {
                                        ret = wc.analiserRequete();
                                    } catch (IOException ex) {
                                        Logger.getLogger(InterfaceRecherche.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    
                                    if(ret.equals("")){
                                        JOptionPane.showMessageDialog(this, "Aucun résultat trouvé pour "+mot_cle.toUpperCase()+" !", "Information",JOptionPane.INFORMATION_MESSAGE);
                                        return;
                                    }
                                    else{
                                        lienFormation = ret;
                                        if(relation.equals("is a property of")){
                                            attribut = domaine1;
                                            domaine = domaine2;
                                        }
                                        else{
                                            domaine = domaine1;
                                        }
                                    }
                                    
                                }
                                
                            }
                        }
                        
                        System.out.println("domaine1 = "+domaine1);
                        System.out.println("relation = "+relation);
                        System.out.println("domaine2 = "+domaine2);
                        //return ;

                    }
                    else{
                        jLabel2.setText("Recherche de plus de deux mots en cours !!!!!!");
                    }
                }

                if( ! lienFormation.equals("")){
                    //Mettre le premier caractère en MAJ
                    //domaine.substring(0, 1).toUpperCase() + domaine.substring(1);
                    if( ! attribut.equals("")){
                        noms_attributs = new ArrayList<>();
                        noms_attributs.add(attribut);
                    }
                    else{
                        noms_attributs = ontologie.getAttributes(domaine.substring(0, 1).toUpperCase() + domaine.substring(1));
                    }
                    //ArrayList<String> noms_attributs = ontologie.getAttributes("Formation");
                    comments = ReaderFile2.getContentFile("ontologie.txt", noms_attributs);
                    FormationWrapper fr = new FormationWrapper(null, noms_attributs, comments, lienFormation);
                    String res = "";
                    
                    try {
                        res = fr.parser();
                    } catch (IOException ex) {
                        Logger.getLogger(InterfaceRecherche.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    String msg = "<html>Résultat de recherche pour <font color=red>"+mot_cle.toUpperCase()+"</font> </html>:";
                    jLabel3.setText(msg);
                    jLabel2.setText(res);
                    jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                }
                else{
                    JOptionPane.showMessageDialog(this, "Aucun résultat trouvé pour "+mot_cle.toUpperCase()+" !", "Information",JOptionPane.INFORMATION_MESSAGE);
                }
            }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MousePressed

    }//GEN-LAST:event_jButton1MousePressed

    private void jButton1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1MouseReleased

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==10){
            jButton1ActionPerformed(null);
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    /**
     * @param args the command line arguments
     */
   
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}


