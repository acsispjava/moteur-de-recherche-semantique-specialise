package com.mycompany.mavenproject1;

import com.uvsq.vue.InterfaceRecherche;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InterfaceRecherche().setVisible(true);
            }
        });
    }
}
